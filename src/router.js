import VueRouter from 'vue-router';

import HelloWorld from '@/components/HelloWorld';
import ProductsTable from '@/components/ProductsTable';
import AddProductForm from '@/components/AddProductForm';

const router = new VueRouter({ 
  routes: [
    { path: '', component: HelloWorld },
    { path: '/products', component: ProductsTable },
    { path: '/add-product', component: AddProductForm }
  ]
})

export default router